import React, { Component } from 'react';
import './index.css';
import StoriesList from './StoriesList';
import Story from './Story';

import { BrowserRouter as Router, Route, Link } from 'react-router-dom';

class App extends Component {

    render() {
        return (
            <Router>
                <div className="App">
                    <header className="App-header">
                        <h1 className="App-title"><Link to="/">Hacker News</Link></h1>

                       <nav>
                           <ul>
                               <li><Link to="/">Home</Link></li>
                               <li><Link to="/about">About</Link></li>
                           </ul>
                       </nav>

                    </header>

                    <main>
                        <Route exact path="/" component={StoriesList}/>
                        <Route path="/about" component={About}/>
                        <Route path="/item/:storyid" component={Story}/>
                    </main>

                    <footer>
                        <nav>
                            <ul>
                                <li>Guidelines</li>
                                <li>FAQ</li>
                                <li>Support</li>
                                <li>API</li>
                                <li>Lists</li>
                            </ul>
                        </nav>
                        <div>
                            <div className="search">
                                <label htmlFor="search-hn">Search:</label>
                                <input type="text" className="search-hn"/>
                            </div>
                        </div>

                    </footer>



                </div>
            </Router>
        );
    }


}

class About extends Component {

    render() {
        return(
          <div>
              About page! 
          </div>
        );
    }
}



export default App;
