import React, { Component } from 'react';

class AddComment extends Component {

    render() {
        return(
            <div className="add-comment">
                <div>
                    <textarea name="add-comment" id="" cols="60" rows="10"></textarea>
                </div>
                <div className="button-container">
                    <button className="button-add-comment">Add Comment</button>
                </div>
            </div>

        );
    }
}

export default AddComment;