import React, { Component } from 'react';
import axios from 'axios';
import Comment from './Comment';

class Comments extends Component {

    constructor(props) {
        super(props);

        this.state = {
            comments : {}
        };
    }

    componentDidMount() {
        this.fetchComments(this.props.commentIds);
    }

    render() {
        return this.renderComments();
    }


    // Gets the render code for comments and each comment's child comments
    renderComments() {
        let topLevelComments = this.state.comments.childComments,
            allCommentsMarkup = [];

        if (!topLevelComments) {
            return null;
        }

        getCommentsMarkup(topLevelComments, 0);

        function getCommentsMarkup(comments, nestingLevel) {

            if (!comments || !comments.length) {
                return;
            }

            comments.forEach((comment, index) => {
                if (comment.deleted) {
                    return;
                }

                allCommentsMarkup.push(<Comment {...comment} nestingLevel={nestingLevel} key={comment.id.toString()} />);

                getCommentsMarkup(comment.childComments, nestingLevel + 1);
            });
        }

        return allCommentsMarkup;
    }



    // For the given array of comment ids, recursively fetches data for the individual comments and their child comments
    // Note: To limit the number of web service calls made, fetches child comments only to a maximum nesting level of 3.
    fetchComments(commentIds) {
        let comments = {},
            _this = this;

        fetchChildComments(commentIds, comments);


        function fetchChildComments(commentIds, parent) {

            if (!commentIds || !commentIds.length) {
                return;
            }

            if (!parent.childComments) {
                parent.childComments = [];
            }

            commentIds.forEach(function(commentId, index, array) {
                const maxNestingLevel = 2;
                if (index > maxNestingLevel) {
                    return;
                }

                let url = 'https://hacker-news.firebaseio.com/v0/item/' + commentId  + '.json?print=pretty';
                axios.get(url)
                    .then(res => {
                        let comment = res.data;
                        parent.childComments[index] = comment;
                        _this.setState({
                            comments: comments
                        });

                        if (comment.kids) {
                            fetchChildComments(comment.kids, comment);
                        }
                    });
            });
        }

    }

}

export default Comments;