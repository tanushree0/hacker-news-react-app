import React, { Component } from 'react';
import {Link} from 'react-router-dom';
import {getTimeDifference} from "./Utils";

class StoryListItem extends Component {

    render() {
        const props = this.props,
            author = props.by,
            score = props.score,
            title = props.title,
            url = props.url,
            rank = props.rank + 1,
            age = props.time && getTimeDifference(Date.now(), props.time*1000), // props.time is in UNIX time. So it's
        // in seconds, convert to ms to be the same unit as Date.now()
            numComments = props.descendants,
            id = props.id;

        let pathArray = url && url.split('/'),
            hostname = pathArray && pathArray[2];

        //TODO: how to: put the url href only when the variable url has a value
        return(
            <div className="story">
                <div className="row">
                    <span className="rank">{rank || ""} </span>
                    <a className="link-upvote ion-arrow-up-b metadata-item"></a>
                    <a href={url} className="title">{title}</a>
                    <span className="source metadata-item">({hostname})</span>
                </div>
                <div className="row metadata-row">
                    <span className="points metadata-item">{score} points</span>
                    <span className="contributor metadata-item">by {author}</span>
                    <span className="age metadata-item">{age}</span>
                    <Link to={`/item/${id}`} className="num-comments metadata-item">{numComments} comments</Link>
                </div>
            </div>
        );
    }
}

export default StoryListItem;