import React, { Component } from 'react';
import {decodeHtml, getTimeDifference} from './Utils';

class Comment extends Component {
    render() {
        let props = this.props,
            commentText = decodeHtml(props.text),
            nestingLevel = props.nestingLevel,
            age = props.time && getTimeDifference(Date.now(), props.time*1000),
            by = props.by;

        return(
            <div className={`comment nesting-${nestingLevel}`}>
                <div className="metadata-row">
                    <a className="link-upvote ion-arrow-up-b metadata-item"></a>
                    <span className="metadata-item">{by}</span>
                    <span className="metadata-item"> {age}</span>
                </div>
                <div>{commentText}</div>
            </div>

        );
    }
}

export default Comment;