import React, { Component } from 'react';
import axios from 'axios';
import StoryListItem from './StoryListItem';

class StoriesList extends Component {

    constructor(props) {
        super(props);

        let cachedStories = localStorage.getItem("stories"),
            stories;

        if(cachedStories) {
            stories = JSON.parse(cachedStories);
        }

        this.state = {
            stories: stories
        };
    }


    componentDidMount() {
        const _this = this;

        axios.get(`https://hacker-news.firebaseio.com/v0/topstories.json?print=pretty`)
            .then(res => {
                let posts = res.data,
                    topPosts = posts.slice(0,20),
                    promises = [],
                    storiesData = [];

                topPosts.forEach(function(item, index, array) {
                    let url = 'https://hacker-news.firebaseio.com/v0/item/' + item  + '.json?print=pretty';
                    promises[index] = axios.get(url)
                        .then(res => {
                            storiesData[index] = res.data;
                        });
                });

                // Set state once all the web requests have returned
                Promise.all(promises).then(function(values) {
                    _this.setState({stories: storiesData});
                    localStorage.setItem("stories", JSON.stringify(storiesData));
                });
            });
    }



    render() {
        let storiesCode = [],
            stories = this.state.stories;

        if (!stories || !stories.length) {
            return(
                <div className="loading-spinner"></div> //TODO: needs to be a component
            );
        }

        stories.forEach((story, index) => {
            storiesCode.push(<StoryListItem {...story} rank={index} key={story.id.toString()}/>);
        });

        return storiesCode;
    }
}

export default StoriesList;