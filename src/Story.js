import React, { Component } from 'react';
import axios from 'axios';
import StoryListItem from './StoryListItem';
import Comments from './Comments';
import AddComment from './AddComment';


class Story extends Component {

    constructor(props) {
        super(props);

        this.id = Number(this.props.match.params.storyid);

        let cachedStories = localStorage.getItem("stories"),
            story;

        if(cachedStories) {
            let stories = JSON.parse(cachedStories);
            story = stories && stories.find(story => story.id === this.id);
        }

        this.state = {
            story: story
        };
    }

    componentDidMount() {
        this.fetchStory(this.id);
    }

    render() {
        let story = this.state.story;

        if (!story || !story.kids) {
            return null;
        }

        return(
            <div>
                <StoryListItem {...story} />
                <AddComment/>
                <Comments commentIds={story.kids}/>
            </div>
        );
    }

    fetchStory(storyId) {
        const _this = this;
        let url = 'https://hacker-news.firebaseio.com/v0/item/' + storyId  + '.json?print=pretty';

        axios.get(url)
            .then(res => {
                _this.setState({
                    story: res.data
                });
            });
    }
}


export default Story;