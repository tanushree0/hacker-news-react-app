This project was bootstrapped with [Create React App](https://github.com/facebookincubator/create-react-app).

This is a Hacker News app, and is built to learn to use React. 

- To install: npm install
- To run: npm start


### pages 

The app has two main pages: 

1. Home page
- Fetches the top stories for HN. 
- Fetches the story details for the top 20 stories (makes individual web requests)
- Renders the story list 
- Just for the purpose of this test app, stores the stories in localStorage and uses them for faster loading experience. 

2. Story page 
- Fetches the story detail 
- Fetches comments (and their respective child comments) recursively for the story. To limit the number of web requests made, the nesting level of the comments fetched is limited to 3. 
- Renders the story detail, add comment and comments 

### libraries and tools

- Axios for HTTP requests
- React-router-dom for routing
- SASS, Autoprefixer, Gulp



